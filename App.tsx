import React from 'react';
import GraphiQL from 'graphiql';
import 'graphiql/graphiql.min.css';
// @ts-ignore
import autobahn from '../node_modules/autobahn-browser/autobahn';

const config = {
    url: 'ws://127.0.0.1:8080/ws',
    realm: 'default'
}

// @ts-ignore
var wamp_session: Object

async function Connect () {
  const connection = new autobahn.Connection(config)
  connection.onopen = function (session: Object) {
    wamp_session = session
  }
  await connection.open()
}

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

Connect()

const App = () => (
  <GraphiQL
    fetcher={async graphQLParams => {
      while (wamp_session == null) {
        await sleep(100);
      }
      // @ts-ignore
      return await wamp_session.call('graphql.query', [graphQLParams])
    }}
  />
);

export default App;
