
import graphene
from resolver import Resolver


class Episode(graphene.Enum):
    NEWHOPE = 4
    EMPIRE = 5
    JEDI = 6


class Character(graphene.Interface):
    """An Interface to define fields common to multiple actors"""
    id = graphene.ID()
    name = graphene.String()
    friends = graphene.List(lambda: Character)
    appears_in = graphene.List(Episode)

    def resolve_friends(self, info):
        """Resolve a list of friend id's to a list of actor 'objects'"""
        return [resolver.actor_by_id(f) for f in self.friends]


class Human(graphene.ObjectType, interfaces=[Character]):
    """Our human object is a 'Character' with a unique field 'home_planet'"""
    home_planet = graphene.String()


class Droid(graphene.ObjectType, interfaces=[Character]):
    """Our droid object is a 'Character' with a unique field 'primary_function'"""
    primary_function = graphene.String()


class createHuman(graphene.Mutation):
    """To create a Human object"""

    class Arguments:
        name = graphene.String()
        friends = graphene.List(graphene.String)
        appears_in = graphene.List(Episode)
        home_planet = graphene.String()

    human = graphene.Field(Human)

    def mutate(parent, info, name, home_planet=None, friends=[], appears_in=[]):
        human = Human(
            name=name,
            home_planet=home_planet,
            friends=resolver.lookup_friends(friends),
            appears_in=appears_in
        )
        resolver.create_human(human)
        return createHuman(human=human)


class deleteHuman(graphene.Mutation):
    """To delete a Human object (by name)"""

    class Arguments:
        name = graphene.String()

    ok = graphene.Boolean()

    def mutate(parent, info, name):
        resolver.delete_human(name)
        return deleteHuman(ok=True)


class Query(graphene.ObjectType):
    """This is where we present the available queries"""
    hero = graphene.Field(Character, episode=Episode())
    human = graphene.Field(Human, id=graphene.String())
    droid = graphene.Field(Droid, id=graphene.String())
    human_by_name = graphene.Field(graphene.List(Human), name=graphene.String())
    droid_by_name = graphene.Field(graphene.List(Droid), name=graphene.String())

    def resolve_hero(root, info, episode=None):
        return resolver.actor_by_name('R2-D2' if episode == 5 else 'Luke Skywalker')

    def resolve_human(root, info, id):
        return resolver.actor_by_id(id)

    def resolve_droid(root, info, id=None):
        return resolver.actor_by_id(id)

    def resolve_droid_by_name(root, info, name):
        return resolver.actors_by_name(name)

    def resolve_human_by_name(root, info, name):
        return resolver.actors_by_name(name)


class Mutation(graphene.ObjectType):
    """This is where we present the available mutations"""
    create_human = createHuman.Field()
    delete_human = deleteHuman.Field()


resolver = Resolver(Human, Droid)
schema = graphene.Schema(query=Query, mutation=Mutation)
