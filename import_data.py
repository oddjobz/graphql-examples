#!/usr/bin/env python3

from pynndb import Manager, Doc
from schema_pynndb import Episode
from resolver import ActorType

sdb = Manager().database('star-wars')

for table in sdb.tables():
    sdb.table(table)
    sdb.drop(table)

t_actors = sdb.table('actors')
t_actors.ensure('by_name', '{name}')

actors = [
    {'name': 'Luke Skywalker', 'home_planet': 'Tatooine', 'type': ActorType.HUMAN.value},
    {'name': 'Darth Vader', 'home_planet': 'Tatooine', 'type': ActorType.HUMAN.value},
    {'name': 'Han Solo', 'home_planet': None, 'type': ActorType.HUMAN.value},
    {'name': 'Leia Organa', 'home_planet': 'Alderaan', 'type': ActorType.HUMAN.value},
    {'name': 'Wilhuff Tarkin', 'home_planet': None, 'type': ActorType.HUMAN.value},
    {'name': 'C-3PO', 'primary_function': 'Protocol', 'type': ActorType.DROID.value},
    {'name': 'R2-D2', 'primary_function': 'Astromech', 'type': ActorType.DROID.value},
]

friends = [
    {'name': 'Luke Skywalker', 'friends': ['Han Solo', 'Leia Organa', 'C-3PO', 'R2-D2']},
    {'name': 'Darth Vader', 'friends': ['Wilhuff Tarkin']},
    {'name': 'Han Solo', 'friends': ['Luke Skywalker', 'R2-D2']},
    {'name': 'Leia Organa', 'friends': ['Luke Skywalker', 'Leia Organa', 'C-3PO', 'R2-D2']},
    {'name': 'Wilhuff Tarkin', 'friends': ['Darth Vader']},
    {'name': 'C-3PO', 'friends': ['Luke Skywalker', 'Han Solo', 'Leia Organa', 'R2-D2']},
    {'name': 'R2-D2', 'friends': ['Luke Skywalker', 'Han Solo', 'Leia Organa']},
]

appears = [
    {'name': 'Luke Skywalker', 'appears_in': [Episode.NEWHOPE.value, Episode.EMPIRE.value, Episode.JEDI.value]},
    {'name': 'Darth Vader', 'appears_in': [Episode.NEWHOPE.value, Episode.EMPIRE.value, Episode.JEDI.value]},
    {'name': 'Han Solo', 'appears_in': [Episode.NEWHOPE.value, Episode.EMPIRE.value, Episode.JEDI.value]},
    {'name': 'Leia Organa', 'appears_in': [Episode.NEWHOPE.value, Episode.EMPIRE.value, Episode.JEDI.value]},
    {'name': 'Wilhuff Tarkin', 'appears_in': [Episode.NEWHOPE.value]},
    {'name': 'C-3PO', 'appears_in': [Episode.NEWHOPE.value, Episode.EMPIRE.value, Episode.JEDI.value]},
    {'name': 'R2-D2', 'appears_in': [Episode.NEWHOPE.value, Episode.EMPIRE.value, Episode.JEDI.value]},
]

print('Importing')
for actor in actors:
    t_actors.append(Doc(actor))

for entry in friends:
    actor = t_actors.seek_one('by_name', Doc({'name': entry['name']}))
    for friend_name in entry['friends']:
        friend = t_actors.seek_one('by_name', Doc({'name': friend_name}))
        if actor._friends is None:
            actor._friends = [friend.key]
        else:
            actor._friends.append(friend.key)
    t_actors.save(actor)

for entry in appears:
    actor = t_actors.seek_one('by_name', Doc({'name': entry['name']}))
    actor._appears_in = entry['appears_in']
    t_actors.save(actor)

print(f'Complete, records={t_actors.records()}')
