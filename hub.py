#!/usr/bin/env python3

import asyncio
import argparse
from loguru import logger as log
from autobahn.asyncio.component import Component
from schema_pynndb import schema


class Hub:

    def __init__(self, args):
        transports = [{
            'type': 'rawsocket',
            'endpoint': {
                'type': 'tcp',
                'host': args.host,
                'port': args.port
            }
        }]
        self._crossbar = Component(realm=args.realm, transports=transports)
        self._crossbar.on("join", self.on_join)

    async def on_join(self, session, details):
        log.info('Connected to Crossbar with authid "{}" on realm "{}"', details.authid, details.realm)
        session.register(self.graphql_query, 'graphql.query')

    async def run(self):
        self._crossbar.start(loop=asyncio.get_running_loop())
        try:
            while True:
                await asyncio.sleep(1)
        except Exception as e:
            log.exception(e)
        finally:
            pass

    async def graphql_query(self, graphQLParams, details=None):
        return schema.execute(
            graphQLParams.get('query'),
            operation_name=graphQLParams.get('operationName'),
            variables=graphQLParams.get('variables')
        ).to_dict()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--name", dest="name", default="dummy")
    parser.add_argument("--host", dest="host", default="127.0.0.1")
    parser.add_argument("--port", dest="port", default=9000, type=int)
    parser.add_argument("--realm", dest="realm", default="default")
    try:
        log.success('Running')
        asyncio.run(Hub(parser.parse_args()).run())
    except KeyboardInterrupt:
        print()
        log.success('Finished')
