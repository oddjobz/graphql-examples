import graphene
from pynndb import Manager, Doc, exceptions


class ActorType(graphene.Enum):
    HUMAN = 1
    DROID = 2


class Resolver:

    def __init__(self, Human, Droid):
        self._Human = Human
        self._Droid = Droid
        self._t_actors = Manager().database('star-wars').table('actors')

    def actor_by_id(self, id):
        doc = self._t_actors.get(id)
        return self.materialise(doc)

    def actor_by_name(self, name):
        doc = self._t_actors.seek_one('by_name', Doc({'name': name}))
        if not doc:
            raise Exception(f'not found: {name}')
        return self.materialise(doc)

    def actors_by_name(self, name):
        results = []
        for result in self._t_actors.filter('by_name', lower=Doc({'name': name})):
            if not result.doc._name.startswith(name):
                break
            results.append(self.materialise(result.doc))
        return results

    def lookup_friends(self, names):
        results = []
        for name in names:
            doc = self._t_actors.seek_one('by_name', Doc({'name': name}))
            if not doc:
                raise Exception(f'no such friend: {name}')
            results.append(doc.key)
        return results

    def create_human(self, human):
        try:
            self._t_actors.append(Doc({
                'name': human.name,
                'home_planet': human.home_planet,
                'friends': human.friends,
                'appears_in': human.appears_in
            }))
        except exceptions.DuplicateKey:
            raise Exception(f'human called "{human.name}" already exists')

    def delete_human(self, name):
        doc = self._t_actors.seek_one('by_name', Doc({'name': name}))
        if not doc:
            raise Exception(f'not found: {name}')
        self._t_actors.delete(doc)

    def materialise(self, doc):
        if doc._type == ActorType.HUMAN.value:
            return self._Human(
                id=doc.key,
                name=doc._name,
                friends=doc._friends,
                appears_in=doc._appears_in,
                home_planet=doc._home_planet
            )
        else:
            return self._Droid(
                id=doc.key,
                name=doc._name,
                friends=doc._friends,
                appears_in=doc._appears_in,
                primary_function=doc._primary_function
            )
