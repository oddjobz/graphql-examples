# GraphQL Examples

Code to demonstrate how GraphQL can work over Autobahn/WAMP against a Pynndb2 database and has been
set up to provide support materials for the blog post here; https://gareth.bult.co.uk/2021/03/16/graphql/

### GraphiQL

Ok, I'm adding a graphiql folder which contains a compiled version of GraphiQL with an Autobahn transport
which is pre-aimed at a local Crossbar on port 8080. ie. it's set up to work as per the associated blog post. To run, cd into the folder and do;
```
python -m http.server
```
Then point your browser at http://localhost:8000
